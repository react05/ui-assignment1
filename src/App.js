import React from 'react';
import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <pre>
      <Counter></Counter>
      </pre>
    </div>
  );
}


class Counter extends React.Component{

  state={count:0};
  render(){
return (
  <div>
    <h1>Count:{this.state.count}</h1>
    <pre>
    <button onClick={()=>this.setState({count:this.state.count+1})}>Increment</button> <button onClick={()=>this.setState({count:this.state.count-1})}>Decrement</button> 
    </pre>
  </div>
);
}
}

export default App;
